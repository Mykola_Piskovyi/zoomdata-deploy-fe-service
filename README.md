#### STOP
/opt/zoomdata/bin/zdmanage services stop

#### Download and Redeploy
yum -y upgrade zoomdata*

#### START
/opt/zoomdata/bin/zdmanage services start

## Install
```sh
$ npm install
$ npm run build
$ npm run start
```
This will set up a server at 21111 port

## Setup
serverconfig.json file