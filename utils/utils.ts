import { uniqueId } from 'lodash';

export {
	generateCid,
	generateUUID
}

function generateCid(prefix: string): string {
	return uniqueId(`${prefix}_`);
}

function generateUUID(): string {
	return 'xxxx-xxxx-xxxx-xxxx-yxxx'.replace(/[xy]/g, function(c) {
        const r = Math.random() * 16 | 0;
        const v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}