
export type TCommand = 'STOP'|'INSTALL'|'START';

const SEC = 1000;
const SETTINGS = {

	SERVER_FILE_STATE: '',

	TARGET_HOST_TO_PING_DELAY_SEC: 10 * SEC, 
	PERIOD_TO_CHECK_ALIVE: 15 * SEC,

	/**
	 * http://fe.zoomdata.com:8080
	 */
	TARGET_HOST_TO_PING: '',

	/**
	 * http://fe.zoomdata.com:8080/zoomdata/service/version
	 */
	TARGET_HOST_VERSION: '',

	BAMBOO_MASTER_BUILD_REST: '',

	/**
	 * REDEPLOY_COMMDANDS_ORDER: ['STOP', 'REDEPLOY', 'START'] as Array<TCommand>,
	 */
	REDEPLOY_COMMDANDS_ORDER: [],

	/**
	 * COMMANDS: {
	 *		STOP: '/opt/zoomdata/bin/zdmanage services stop',
	*		REDEPLOY: 'yum -y upgrade zoomdata*',
	*		START: '/opt/zoomdata/bin/zdmanage services start'
	*	},
	*/
	COMMANDS: {},

	TEXT: {
		REDEPLOY_COMPLETE: 'Redeploy has been completed'
	}
}

export {
	getSettings,
	setJsonSettings
};

function getSettings() {
	return SETTINGS;
}

function setJsonSettings(json) {
	if (typeof json.commands === 'object') {
		SETTINGS.COMMANDS = json.commands
	}
	if (Array.isArray(json.commandsOrder)) {
		SETTINGS.REDEPLOY_COMMDANDS_ORDER = json.commandsOrder;	
	}
	if (typeof json.targetServerToPing === 'string') {
		SETTINGS.TARGET_HOST_TO_PING = json.targetServerToPing;		
	}
	if (typeof json.targetServerVersion === 'string') {
		SETTINGS.TARGET_HOST_VERSION= json.targetServerVersion;		
	}
	if (typeof json.bambooMasterPlanRest === 'string') {
		SETTINGS.BAMBOO_MASTER_BUILD_REST = json.bambooMasterPlanRest;
	}
	if (typeof json.serverFileState === 'string') {
		SETTINGS.SERVER_FILE_STATE = json.serverFileState;
	}

	return SETTINGS;
}