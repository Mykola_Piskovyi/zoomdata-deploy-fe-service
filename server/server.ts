'use strict';

import * as express from 'express';
import * as WebSocket from 'ws';
import * as path from 'path';
import * as http from 'http';
import * as fs from 'fs';
import * as bodyParser from 'body-parser';
const ip = require('ip');

import {setJsonSettings} from '../settings/SETTINGS';
import ServerWebSocket from './service/ServerWebSocket';
import ServerState from './service/ServerState';
import {readConfigFileSettings} from './service/readConfigFileSettings';

import routes from './routes';

const app = express();
const server = http.createServer(app);

app.use(express.static('dist'));
app.use(bodyParser.json());
app.use(routes);

readConfigFileSettings((data) => {
    const json = JSON.parse(data);
    const SETTINGS = setJsonSettings(json);
    const PORT = json.startPort as number;

    server.listen(PORT, () => console.log(`Server started on "http://${ip.address()}:${PORT}"`));
    
    ServerState.create();
    const socket = ServerWebSocket.getInstance({
        server, 
        path: '/ws', 
        host: `${json.serverHost}:${PORT}`,
        onStart: (ws) => {
            console.log(`WebSocket started on "${ws.getHost()}"`);
        }
    });
}, (err) => console.error(err));
