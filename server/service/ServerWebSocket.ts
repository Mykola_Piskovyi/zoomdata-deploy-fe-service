'use strict';

import { assign } from 'lodash';
import * as WebSocket from 'ws';
import * as path from 'path';
import * as http from 'http';
import Timeout from 'timeout-exec';

import {getSettings, TCommand} from '../../settings/SETTINGS';

import {isTargetAlive} from './isTargetAlive';

let ws: ServerWebSocket|null = null;

const EVENTS = {
    CONNECTION: 'connection',
    MESSAGE: 'message',
    ERROR: 'error'
};

type TSocketMessageDataType = 'COMMAND' | 'COMMAND_LOG';
type TSocketMessageDataStatus = 'RUN' | 'DONE' | 'ERROR';

export type TCommandMessageData = {
    uuid: string;
    type?: TSocketMessageDataType;
    status?: TSocketMessageDataStatus;
    error?: string;
    data: {
        command: string;
        commandName: TCommand;
        text: string;
    };
};

export type TTargetAliveMessageData = {
    type: 'TARGET_ALIVE';
    status: boolean;
    text: string;
};

export type TSocketMessageData = TCommandMessageData | TTargetAliveMessageData;

interface WSOptions {
    server: http.Server;
    host: string;
    path: string;
    onStart(ServerWebSocket): void;
}

export default class ServerWebSocket {

    static EVENTS = EVENTS;
    static getInstance = getInstance;
    static destroyInstance = destroyInstance;

    private cid: string;
    private socket: WebSocket.Server;
    private host: string;
    on: (...args: any[]) => void;
    
    EVENTS = EVENTS;
    getInstance = getInstance;
    destroyInstance = destroyInstance;

    private timeout = new Timeout();

    constructor(options: WSOptions) {
        if (ws !== null) {
            return ws;
        } else {
            ws = this;
        }

        this.cid = String(Date.now());
        this.socket = new WebSocket.Server({
            server: options.server,
            path: options.path || '/ws'
        });
        this.host = options.host;

        if (typeof options.onStart === 'function') {
            options.onStart(this);
        }

        const SETTINGS = getSettings();

        this.socket
            .on(EVENTS.CONNECTION, (ws) => onConnection(ws, this.socket.clients, this))
            .on(EVENTS.MESSAGE, (data) => onMessageBroadcast(data, this.socket.clients))
            .on(EVENTS.ERROR, (err) => onError(err));

        this.timeout
            .interval(SETTINGS.PERIOD_TO_CHECK_ALIVE)
            .execute(() => isTargetAlive(this));
        
    }

    getHost() {
        return `ws://${this.host}${ws.getPath()}`;
    }

    getPath() {
        return this.socket.options.path;
    }

    send(data: TSocketMessageData) {
        this.socket.emit(EVENTS.MESSAGE, this.createMessage(data));
    }

    createMessage(data: TSocketMessageData) {
        return message(data);
    }

    destroy() {
        this.socket.removeAllListeners();
        this.socket.close();
        this.timeout.destroy();
    }

}

function getInstance(options?: WSOptions): ServerWebSocket {
    if (ws === null) {
        ws = new ServerWebSocket(options);
    }
    return ws;
};

function destroyInstance() {
    if (ws !== null) {
        ws.destroy();
        ws = null;
    }
}

function message(data: TSocketMessageData): string  {
    return JSON.stringify(data);
}

function onConnection(ws: WebSocket, clients: Set<WebSocket>, socket: ServerWebSocket) {
    isTargetAlive(socket);
}

function onMessageBroadcast(data: string, clients: Set<WebSocket>) {
    if (typeof data !== 'string') {
        return;
    }
    clients.forEach((client) => client.send(data));
}

function onError(err) {

}
