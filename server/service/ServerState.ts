import * as fs from 'fs';
import * as path from 'path';
import { observable, action, observe, toJS, extendObservable, IObservable, IObservableObject, Lambda } from 'mobx';
import {getSettings} from '../../settings/SETTINGS';

let state: ServerState|null = null;

interface IServerState {
    lastRedeployTime: number|null;
}

const DEFAULT_STATE: IServerState = {
    lastRedeployTime: null
};

export default class ServerState {

    static create = create;
    static getInstance = getInstance;
    static destroyInstance = destroyInstance;
    getInstance = getInstance;
    destroyInstance = destroyInstance;

    disposers: Array<Lambda> = [];

    state = observable.object(DEFAULT_STATE);

    constructor() {
        const FILE_STATE = getSettings().SERVER_FILE_STATE;
        readAndSetState(FILE_STATE, this.state);
        const watchDisposer = observe(this.state, () => saveState(FILE_STATE, toJS(this.state)));
        this.disposers.push(watchDisposer);
    }

    @action setLastRedeployTime(value: number) {
        this.state.lastRedeployTime = value;
    }

    getLastRedeployTime(): number {
        return this.state.lastRedeployTime;
    }

}

function create(): ServerState {
    if (state === null) {
        state = new ServerState();
    }
    return state;
}

function getInstance(): ServerState {
    return create();
};

function destroyInstance() {
    if (state !== null) {
        state.disposers.forEach(dispose => dispose());
        state.disposers = [];
        state = null;
    }
}

function saveState(FILE_STATE, state: IServerState) {
    fs.writeFile(FILE_STATE, JSON.stringify(state), (err) => {
        if (err) {
            console.error(`Failed to save file: "${FILE_STATE}"`, err);    
        }
    });
}

function readAndSetState(FILE_STATE, state: IObservableObject): void {
    fs.readFile(FILE_STATE, 'utf8', (err, data) => {
        if (err) {
            fs.mkdirSync(path.dirname(FILE_STATE));
        }
        if (data) {
            const json = JSON.parse(data);
            extendObservable(state, json);
        }
    });
}