import * as fetch from 'node-fetch';
import {debounce} from 'lodash';
import {getSettings} from '../../settings/SETTINGS';
import ServerWebSocket from './ServerWebSocket';

export {
	isTargetAlive
}

let request = null;

function isTargetAlive(socket: ServerWebSocket) {
	if (request) {
		return request;
	}

	const SETTINGS = getSettings();

	const socketSendTargetAlive = (alive: boolean) => {
		socket.send({
			type: 'TARGET_ALIVE',
			status: alive,
			text: `${SETTINGS.TARGET_HOST_TO_PING}`
		});

		request = null;
	}

	request = fetch(SETTINGS.TARGET_HOST_TO_PING, {
		method: 'GET',
		timeout: SETTINGS.TARGET_HOST_TO_PING_DELAY_SEC
	})
		.then(() => socketSendTargetAlive(true))
		.catch(() => socketSendTargetAlive(false));

	return request;
}