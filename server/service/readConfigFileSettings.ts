import * as fs from 'fs';
import * as path from 'path';
const settingsFileJson = path.resolve(__dirname, '../../serverconfig.json');

export {
	readConfigFileSettings
}

function readConfigFileSettings(onSuccess: Function, onError?: Function) {
	fs.readFile(settingsFileJson, 'utf8', (err, data) => {
		if (err) {
			onError(err);
		} else {
			onSuccess(data);
		}
	});
}