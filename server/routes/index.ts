'use strict';

import * as path from 'path';
import {Express, Router} from 'express';

import applyDeployRoutes from './Deploy';

const routes = Router();

// routes.use(utils.router.allowCrossDomain);

routes.get('/', (req, resp, next) => onLoadIndex(resp, next));
routes.use('/deploy', applyDeployRoutes);

export default routes;

function onLoadIndex(resp, next) {
    const filePath = path.resolve(__dirname, '../index.html');
    resp.sendFile(filePath);
};
