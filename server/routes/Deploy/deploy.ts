'use strict';

import { Router, Request } from 'express';
import * as shell from 'shelljs';
import * as fetch from 'node-fetch';

import {generateUUID} from '../../../utils/utils';
import ServerWebSocket, {TCommandMessageData, TSocketMessageData} from '../../service/ServerWebSocket';
import {isTargetAlive} from '../../service/isTargetAlive';
import {readConfigFileSettings} from '../../service/readConfigFileSettings';
import ServerState from '../../service/ServerState';

import {getSettings, TCommand} from '../../../settings/SETTINGS';

const routes = Router();

routes.post('/', (req, res) => (onPostDeploy(req, res)));
routes.get('/version', (req, res) => (onGetVersion(req, res)));
routes.get('/availableVersion', (req, res) => (onGetAvailableVersion(req, res)));
routes.get('/settings', (req, res) => (onGetServerSettings(req, res)));

export default routes;

/**
 * req.body.uuid - command uuid
 * req.body.command - command for run
 * req.body.commandName - command name for run
*/
function onPostDeploy(req, resp) {
    const socket = ServerWebSocket.getInstance();
    const {uuid, command, commandName} = req.body as {uuid: string; command: string; commandName: TCommand};

    const socketSendData = (data: TSocketMessageData, checkAlive = true) => {
        socket.send(data);
        if (checkAlive) {
            isTargetAlive(socket);    
        }
    };

    socketSendData({
        uuid: uuid,
        type: 'COMMAND',
        status: 'RUN',
        data: {
            commandName,
            command, 
            text: command
        },
    });

    const child = shell.exec(command, (code, stdout, stderr) => {
        const json = {
            code,
            uuid,
            stdout,
            stderr
        };

        const socketData = {
            uuid: uuid,
            status: 'DONE',
            data: {
                commandName,
                command, 
                text: ''
            }
        } as TCommandMessageData;

        if (stderr) {
            resp.status(400).json(json);
            socketData.error = stderr;
        } else {
            resp.status(200).json(json);
            if (commandName === 'INSTALL') {
                ServerState.getInstance().setLastRedeployTime(Date.now());
            }
        }

        socketSendData(socketData);
    });

    child.stdout.on('data', (data) => socketSendData({
        uuid: uuid,
        type: 'COMMAND_LOG',
        status: 'RUN',
        data: {
            commandName,
            command,
            text: data
        }
    }, false));
}

function onGetServerSettings(req, resp) {
    const ws = ServerWebSocket.getInstance().getHost();
    readConfigFileSettings(
        (data) => resp.status(200).json({...JSON.parse(data), ws: ws}),
        (err) => resp.status(400).json(err)
    );
}

function onGetVersion(req, resp) {
    const SETTINGS = getSettings();

    return fetch(SETTINGS.TARGET_HOST_VERSION)
        .then(data => data.json())
        .then((data) => {
            resp.status(200).json({
                ...data, 
                lastRedeployTime: ServerState.getInstance().getLastRedeployTime()
            });
        })
        .catch(err => onError(resp, err));
}

function onGetAvailableVersion(req, resp) {
    const SETTINGS = getSettings();
    return fetch(SETTINGS.BAMBOO_MASTER_BUILD_REST, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
        .then(data => data.json())
        .then((data) => {
            resp.status(200).json(data);
        })
        .catch(err => onError(resp, err));
}

function onError(resp, error) {
    console.log('ERROR', JSON.stringify(error));
    resp.sendStatus(400, error);
}
