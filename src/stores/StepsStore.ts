import {observable, action, observe, computed, Lambda} from 'mobx';
import {last} from 'lodash';

import {getSettings, TCommand} from '../../settings/SETTINGS';
import AppSocket from '../service/AppSocket';

export default class StepsStore {

	disposerProgress: Lambda;
	disposerCommand: Lambda;

	commands: Array<TCommand> = getSettings().REDEPLOY_COMMDANDS_ORDER.slice();

	@observable currentCommand: TCommand|null = null;
	@observable progress: boolean = false;
	@observable error: any = null;
	@observable disabled: boolean = false;
	@observable nextCommand: TCommand = this.commands[0];
	@observable finished: boolean = false;

	constructor() {
		this.disposerProgress = observe(this, 'progress', onProgressChange.bind(this));
		this.disposerCommand = observe(this, 'currentCommand', onCurrentCommandChange.bind(this));

		function onProgressChange(change) {
			if (change.oldValue === true && change.newValue === false) {
				this.nextCommand = this.commands[this.getActiveStep(this.currentCommand) + 1];
			}

			if (change.newValue === false && this.currentCommand === last(this.commands)) {
				this.finished = true;
			}
		}

		function onCurrentCommandChange(change) {
			if (change.oldValue !== change.newValue) {
				this.nextCommand = this.commands[this.getActiveStep(this.currentCommand)];
				this.finished = false;
			}
		}
	}

	@action setCommand(command: TCommand): this {
		this.currentCommand = command;
		return this;
	}

	@action setProgress(value: boolean): this {
		this.progress = value;
		return this;
	}

	getActiveStep(command?: TCommand): number {
		return this.finished ?
			this.commands.length :
			this.commands.indexOf(command || this.nextCommand);
	}

	@action switchToCommand(command: TCommand) {
		this.currentCommand = command;
		this.nextCommand = command;
		this.error = null;
	}

	@action setDisabled(value: boolean) {
		this.disabled = value;
	}

	@action setError(command, error) {
		this.error = {
			command, msg: error
		};
	}
}
