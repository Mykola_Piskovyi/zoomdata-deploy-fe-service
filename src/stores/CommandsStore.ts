import {observable, action, IObservableArray} from 'mobx';
import Command, { ICommandOpts } from '../models/Command';
import { generateCid, generateUUID } from '../../utils/utils';

export default class CommandsStore {

    static create = create;
    static destroy = destroy;

    cid: string;
    @observable commands: IObservableArray<Command>;

    constructor() {
        this.cid = generateCid('store');
    }

    @action addCommand(command: string, commandName: string): Command {
        const item = new Command(this, {cmd: command, commandName});
        this.commands.push(item);
        return item;
    }

    @action addCommandByUuid(command: string, uuid: string, commandName: string): Command {
        const item = new Command(this, {cmd: command, uuid: uuid, commandName});
        this.commands.push(item);
        return item;
    }

    @action removeCommand(command: Command): this {
        this.commands.remove(command);
        return this;
    }

    @action clearAllLogs() {
        this.commands.forEach(command => command.clearLogs());
    }

    getCommands() {
        return this.commands;
    }

    getCommandByUuid(commandUUID: string): Command|undefined {
        return this.commands.find(item => (item.uuid === commandUUID));
    }

    destroy() {
        destroy();
    }

}

function create(array: Array<ICommandOpts>): CommandsStore {
    const store = new CommandsStore();
    store.commands = observable(
        array.map(item => Command.create(store, {cmd: item.cmd, commandName: item.commandName}))
    );
    return store;
}

function destroy() {
    
}