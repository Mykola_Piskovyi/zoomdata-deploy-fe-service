import {observable, action} from 'mobx';

type TargetVersion = {
    git: string;
    version: string;
    revision: string;
    buildTime: string;
};

export default class AppStore {

    @observable targetVersion: TargetVersion = null;
    @observable targetIsAlive: boolean = false;
    @observable availableVersion: string;
    @observable lastRedeployTime: number|undefined;
    
    @action setVersion(data: TargetVersion) {
        this.targetVersion = data;
    }

    @action setTargetIsAlive(value: boolean) {
        this.targetIsAlive = value;
    }

    @action setAvailableVersion(buildKey: string) {
        this.availableVersion = buildKey;
    }

    @action setLastRedeployTime(value: number) {
        this.lastRedeployTime = value;
    }
}