import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {chain} from 'lodash';
import {useStrict, observe, Reaction} from 'mobx';

useStrict(true);

import AppSocket from './service/AppSocket';
import { App } from './App';
import CommandsStore from './stores/CommandsStore';
import StepsStore from './stores/StepsStore';
import AppStore from './stores/AppStore';
import {setJsonSettings} from '../settings/SETTINGS';

import Rest from './service/Rest';

Rest.get('deploy/settings')
    .then((json) => {
        const WS_HOST = json.ws;
        const PORT = json.startPort;

        const SETTINGS = setJsonSettings(json);

        const commandsStore = CommandsStore.create([]);
        const stepsStore = new StepsStore();
        const appStore = new AppStore();

        ReactDOM.render(
            React.createElement(App, {commandsStore, stepsStore, appStore}),
            document.getElementById('app-view')
        );
        
        window['store'] = {
            commandsStore, stepsStore, appStore
        };

        runObservers(appStore);
        getAvailableVerion(appStore);

        const socket = AppSocket.getInstance({target: WS_HOST});
        socket.on(socket.EVENTS.WS.OPEN, () => socket.send('connected'));
        socket.setCommandsStore(commandsStore, stepsStore, appStore);
    });

function runObservers(appStore: AppStore) {
    observe(appStore, 'targetIsAlive', (change) => getTargetVerion(appStore));
}

function getTargetVerion(appStore: AppStore) {
    Rest.get('deploy/version')
        .then(({lastRedeployTime, ...versionData}) => {
            appStore.setLastRedeployTime(lastRedeployTime);
            appStore.setVersion(versionData);
        });
}

function getAvailableVerion(appStore) {
    Rest.get('deploy/availableVersion')
        .then((data: any) => {
            const key = chain(data.results)
                .get('result')
                .filter(item => item.state === 'Successful')
                .first()
                .get('key')
                .value();
            
            appStore.setAvailableVersion(key);
        });
}