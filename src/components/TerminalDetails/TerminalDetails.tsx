import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {observer} from 'mobx-react';
import {Card, CardHeader, CardText, CardActions, RaisedButton} from 'material-ui';

import CommandsStore from '../../stores/CommandsStore';
import CommandLog from '../CommandLog/CommandLog';

import './TerminalDetails.scss';

interface TerminalDetailsProps {
	store: CommandsStore;
}

@observer
export default class TerminalDetails extends React.Component<TerminalDetailsProps, {}> {

	private _container: CardText;

	componentDidUpdate() {
		this._scrollToEnd();
	}

	componentDidMount() {
		this._scrollToEnd();
	}

	_scrollToEnd() {
		const el = ReactDOM.findDOMNode(this._container) as HTMLElement;

		if (el.clientHeight < el.scrollHeight) {
			el.lastElementChild
				.lastElementChild
				.scrollIntoView({
					block: 'end',
					behavior: 'smooth'
				});
				
		}
	}

	render() {
		const {store} = this.props;
		const cmdLogs = store.commands.map((command) => CommandLog({command}));

		const clearLogs = () => store.clearAllLogs();

		const containerStyle = {
			paddingBottom: 0
		};

		return (
			<Card className='TerminalDetails' containerStyle={containerStyle}>
				<CardHeader title='Terminal Details' children={<RaisedButton className='clear-logs-btn' label='Clear Logs' onClick={clearLogs}/>}/>
				<CardText className='terminal-container' ref={(el) => (this._container = el)}>
					<pre className='terminal-info-formatted' children={cmdLogs}/>
				</CardText>
			</Card>
		)
	}

}