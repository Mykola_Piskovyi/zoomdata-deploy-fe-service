import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {TextField, RaisedButton, RefreshIndicator, CircularProgress} from 'material-ui';
import {observer} from 'mobx-react';

import {getSettings, TCommand} from '../../../settings/SETTINGS';
import CommandsStore from '../../stores/CommandsStore';
import StepsStore from '../../stores/StepsStore';
import AppStore from '../../stores/AppStore';

import TargetDetails from '../TargetDetails/TargetDetails';
import TerminalDetails from '../TerminalDetails/TerminalDetails';
import ProgressIndicator from '../ProgressIndicator/ProgressIndicator';
import CommandButtons from './CommandButtons';

import './RedeployView.scss';

interface RedeployProps {
	commandsStore: CommandsStore;
	stepsStore: StepsStore;
	appStore: AppStore;
}

@observer
export default class RedeployView extends React.Component<RedeployProps, {}> {

	_rightEl: Element;

    _onRunCommand(command: TCommand) {
		const SETTINGS = getSettings();
		const shellCommand = SETTINGS.COMMANDS[command];
		const {commandsStore, stepsStore} = this.props;
		const item = commandsStore.addCommand(String.fromCharCode(10) + shellCommand, command);

		stepsStore.setCommand(command).setProgress(true);

		const onFinish = (err?) => {
			stepsStore.setProgress(false);
			if (err) {
				stepsStore.setError(command, err);
			}
		}

        item.run()
            .then((resp) => onFinish())
            .catch((err) => onFinish(err));
	}

	render() {

		const {commandsStore, stepsStore} = this.props;

		return (
			<div className='RedeployView'>
				<div className='left-section'>
					<TargetDetails appStore={this.props.appStore}/>
					<CommandButtons stepsStore={stepsStore} onCommandRun={(cmd) => this._onRunCommand(cmd)}/>
					{ stepsStore.progress ? (
						<div>
							<ProgressIndicator progress={stepsStore.progress}/>
							<code children={stepsStore.currentCommand}/>
						</div>
						) : null }
				</div>
				<div className='right-section' ref={(el) => {this._rightEl = el}}>
					<TerminalDetails store={this.props.commandsStore} />
				</div>
			</div>
		)
	}

}
