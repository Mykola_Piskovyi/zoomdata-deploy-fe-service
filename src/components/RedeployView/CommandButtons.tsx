import * as React from 'react';
import {capitalize, isEmpty} from 'lodash';
import {observer} from 'mobx-react';
import {Step,Stepper, StepLabel, StepButton, StepContent, RaisedButton, StepProps, StepButtonProps} from 'material-ui';
import WarningIcon from 'material-ui/svg-icons/alert/warning';
import {colors} from 'material-ui/styles';

import {getSettings, TCommand} from '../../../settings/SETTINGS';
import StepsStore from '../../stores/StepsStore';

interface CommandButtonsProps {
	stepsStore: StepsStore;
	onCommandRun(command: TCommand);
}

const CommandButtons: React.SFC<CommandButtonsProps> = ({stepsStore, onCommandRun}) => {
	const activeStep = stepsStore.getActiveStep();
	const error = stepsStore.error || {};

	const onSetStep = (commandName) => {
		stepsStore.switchToCommand(commandName);
	};

	return (
		<div>
			<Stepper activeStep={activeStep} orientation='vertical'>
				{getSteps({
					stepsStore,
					onSetStep, 
					onCommandRun
				})}
			</Stepper>
		</div>
	);
}

export default observer(CommandButtons);

function getSteps({stepsStore, onSetStep, onCommandRun}) {
	const error = stepsStore.error || {};
	const disabled = stepsStore.disabled;
	const inProgress = stepsStore.progress;

	const buttonProps = {
		label: 'Run',
		primary: true,
		disabled: disabled,
		style: {margin: '6px'}
	};

	const {COMMANDS} = getSettings();

	return stepsStore.commands.map((commandName, i) => {
		const cmd = COMMANDS[commandName];
		const onRunClick = () => onCommandRun(commandName);
		const isError = error.command === commandName;

		const buttonProps = {
			children: capitalize(commandName),
			disabled: inProgress || disabled,
			onClick: () => onSetStep(commandName)
		} as StepButtonProps;

		if (isError) {
			buttonProps.icon = <WarningIcon color={colors.red500} />;
		}
		
		const children = [
			<StepButton {...buttonProps} />
		];

		if (!disabled && isEmpty(error)) {
			children.push(getContent(cmd, inProgress, onRunClick));
		}

		return <Step key={commandName} disabled={disabled} children={children}/>
	});
}

function getContent(cmd, inProgress, onRunClick) {
	const buttonProps = {
		label: 'Run',
		primary: true,
		disabled: inProgress,
		style: {margin: '6px'},
		onClick: () => onRunClick()
	};

	return (
		<StepContent style={{paddingTop: 16}}>
			<code style={{marginLeft: '6px'}}>{cmd}</code>
			<br/>
			<RaisedButton {...buttonProps}/>
		</StepContent>
	)
}