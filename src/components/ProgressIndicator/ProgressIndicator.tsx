import * as React from 'react';
import {TextField, RaisedButton, RefreshIndicator, CircularProgress} from 'material-ui';

import './ProgressIndicator.scss';

interface ProgressIndicatorProps {
	progress: boolean;
}

const ProgressIndicator: React.SFC<ProgressIndicatorProps> = ({progress}) => {
	const size = 30;
	return (
		<div className='ProgressIndicator'>
			{ progress ? <CircularProgress size={size} /> : <div style={{height: size, width: size}}/> }
		</div>
	);
}

export default ProgressIndicator;