import * as React from 'react';
import {observer} from 'mobx-react';
import {Card, CardHeader, CardText, CardActions, RaisedButton} from 'material-ui';

import AppStore from '../../stores/AppStore';
import {getSettings} from '../../../settings/SETTINGS';

import './TargetDetails.scss';

interface TargetDetailsProps {
	appStore: AppStore;
}

const DetailItem = (props) => (<div className='details-item' {...props}/>);

const TargetDetails: React.SFC<TargetDetailsProps> = ({appStore}) => {
	const SETTINGS = getSettings();
	const {targetIsAlive, targetVersion, availableVersion, lastRedeployTime} = appStore;
	const statusLabel = targetIsAlive ? 'Up' : 'Down';
	const lastRedeploy = lastRedeployTime ? getReadableTime(lastRedeployTime) : null;
	let version = '';
	let revision = '';
	let buildTime = '';
	let git = '';

	if (targetVersion !== null) {
		version = targetVersion.version;
		revision = targetVersion.revision;
		buildTime = getReadableBuildTime(targetVersion.buildTime);
		git = targetVersion.git;
	}

	const cardTextStyle = {paddingBottom: 0, paddingTop: 0};

	return (
		<Card className='TargetDetails'>
			<CardHeader title='Target Details'/>
			{ availableVersion ? 
				<CardText style={cardTextStyle}>
					<DetailItem>
						<b>Last Successful Master: </b>
						<span><a href={`https://bamboo.zoomdata.com/browse/${availableVersion}`} target='blank'>{availableVersion}</a></span>
					</DetailItem>
				</CardText> 
				: null }
			{ lastRedeploy ?
				<CardText style={cardTextStyle}>
					<DetailItem>
						<b>Last Redeploy:</b> <span>{lastRedeploy}</span>
					</DetailItem>
				</CardText> 
				: null }
			<CardText>
				<DetailItem>
					<b>Host:</b> <a href={SETTINGS.TARGET_HOST_TO_PING} target='blank'>{SETTINGS.TARGET_HOST_TO_PING}</a>
				</DetailItem>
				<DetailItem>
					<b>Status:</b> <span>{blickIcon(targetIsAlive)} {statusLabel}</span>
				</DetailItem>
				<DetailItem>
					<b>Version:</b> <span>{version}</span>
				</DetailItem>
				<DetailItem>
					<b>Revision:</b> <span>{revision}</span>
				</DetailItem>
				<DetailItem>
					<b>Build Time:</b> <span>{buildTime}</span>
				</DetailItem>
				<DetailItem>
					<b>Git commit:</b> <span>{git}</span>
				</DetailItem>
			</CardText>
		</Card>
	)
}

export default observer(TargetDetails);

function blickIcon(alive) {
	const status = alive ? 'green' : 'red';
	return (
		<div className={`blick-icon ${status}`}>
		</div>
	)
}

// 12180705 -> Dec 18 09:05
function getReadableBuildTime(buildTime): string {
	const [month, date, hh, mm] = String(buildTime).match(/(..?)/g).map(Number);
	const time = new Date();

	time.setUTCMonth(month - 1);
	time.setUTCDate(date);
	time.setUTCHours(hh);
	time.setUTCMinutes(mm);

	const localDate = time.toDateString()
		.split(' ')
		.splice(1, 2)
		.join(' ');

	const [localFullTime, gmt, zone] = time.toTimeString().split(' ');
	const localTime = localFullTime
		.split(':')
		.splice(0, 2)
		.join(':');

	return [localDate, localTime, zone].join(' ');
}

function getReadableTime(data: number) {
	const [month, date, hh, mm] = new Date(data).toISOString()
		.split('T')
		.reduce((res, value, i) => {
			if (i === 0) {
				const [year, month, date] = value.split('-');
				res.push(month, date);
			}
			if (i === 1) {
				const [hh, mm, ss] = value.split(':');
				res.push(hh, mm);
			}
			return res;
		}, [])

	return getReadableBuildTime([month, date, hh, mm].join(''));
}