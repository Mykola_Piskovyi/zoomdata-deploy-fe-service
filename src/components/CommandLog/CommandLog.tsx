import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {observer} from 'mobx-react';

import Command from '../../models/Command';

import './CommandLog.scss';

interface ICommandLogProps {
	command: Command;
}

const CommandLog: React.SFC<ICommandLogProps> = ({command}) => {
	return (
		<pre className='CommandLog command-info' key={command.cid}>
			<pre className='command-name'>{command.cmd}</pre>
			<pre className='command-log'>{command.getLogs()}</pre>
		</pre>
	)
}

export default CommandLog;
