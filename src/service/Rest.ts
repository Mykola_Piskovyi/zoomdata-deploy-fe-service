import {noop, assign, isEmpty} from 'lodash';
// import * as qs from 'querystring';

const HEADERS = {
    ACCEPT: {'Accept': 'application/json'},
    CONTENT_JSON: {'Content-Type': 'application/json'},
    ACCESS_ORIGIN: {'Access-Control-Allow-Origin': '*'}
};

// const queryParams = (params) => (qs.stringify(params));
// const urlWithParams = (url, params) => (url + '?' + queryParams(params));
const urlWithParams = (url, params) => (url + '?');

type RestResponce = Promise<any>;

interface RequestOptions extends RequestInit {
	urlParams?: Object;
	[key: string]: any;
}

export default class Rest {
    static get = get;
    static post = post;
    static put = put;
    static delete = remove;

    get = get;
    post = post;
    put = put;
    delete = remove;
}

function get(uri, options: RequestOptions = {}) {
    const getOpts: RequestInit = assign({}, options, {
        method: 'GET',
        headers: assign({}, HEADERS.ACCEPT)
    });

    const url = isEmpty(options.urlParams) ? uri : urlWithParams(uri, options.urlParams);

    return fetch(url, getOpts)
        .then(handleError)
        .then(resp => (resp.json()));
}

function post(uri, options: RequestOptions = {}) {
    const postOpts: RequestInit = assign({}, options, {
        method: 'POST',
        headers: assign({}, HEADERS.ACCEPT, HEADERS.CONTENT_JSON),
        body: options.body ? JSON.stringify(options.body) : ''
    });

    return fetch(uri, postOpts)
        .then(handleError)
        .then(resp => (resp.json()));
}

function put(uri, options: RequestOptions = {}) {
    const putOpts = assign({}, options, {
        method: 'PUT',
        headers: assign({}, HEADERS.ACCEPT)
    });
    return fetch(uri, putOpts)
        .then(handleError)
        .then(resp => (resp.json()));
}

function remove(uri, options: RequestOptions = {}) {
    const deleteOpts = assign({}, options, {
        method: 'DELETE',
        headers: assign({}, HEADERS.ACCEPT)
    });
    return fetch(uri, deleteOpts)
        .then(handleError)
        .then(resp => (resp.json()));
}

function handleError(resp) {
    return resp.ok ?
        resp :
        resp.text().then(text => {
            return Promise.reject(text)
        });
}

function onCatch(error) {
    throw new Error(error);
}
