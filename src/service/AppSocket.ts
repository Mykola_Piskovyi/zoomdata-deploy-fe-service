import { assign } from 'lodash';
import {TCommandMessageData, TSocketMessageData, TTargetAliveMessageData} from '../../server/service/ServerWebSocket';
import CommandsStore from '../stores/CommandsStore';
import StepsStore from '../stores/StepsStore';
import AppStore from '../stores/AppStore';

import {getSettings} from '../../settings/SETTINGS';

let instance = null;

const EVENTS = {
    WS: {
        OPEN: 'open',
        MESSAGE: 'message',
        ERROR: 'error',
        CLOSE: 'close'
    }
}

interface SocketOpts {
    target: string;
}

export default class AppSocket {

    static EVENTS = EVENTS;
    static getInstance = getInstance;
    static destroyInstance = destroyInstance;

    private socket: WebSocket;

    EVENTS = EVENTS;

    /**
    * options.target: string - path to open ws session
    */
    constructor(options: SocketOpts) {
        if (instance !== null) {
            return instance;
        } else {
            instance = this;
        }
        this.socket = new WebSocket(options.target);
    }

    /**
    * event: 'open', 'message', 'error', 'close'
    */
    on(event, subscribeFunc) {
        switch(event) {
            case EVENTS.WS.OPEN:
                this.socket.onopen = (...args) => (subscribeFunc(...args));
                break;
            case EVENTS.WS.MESSAGE:
                this.socket.onmessage = (...args) => (subscribeFunc(...args));
                break;
            case EVENTS.WS.ERROR:
                this.socket.onerror = (...args) => (subscribeFunc(...args));
                break;
            case EVENTS.WS.CLOSE:
                this.socket.onclose = (...args) => (subscribeFunc(...args));
                break;
        }
        return this;
    }

    send(data) {
        this.socket.send(data);
    }

    setCommandsStore(store: CommandsStore, steps: StepsStore, app: AppStore) {
        const getCommand = (msg: TCommandMessageData) => {
            let command = store.getCommandByUuid(msg.uuid);
            if (!command) {
                command = store.addCommandByUuid(msg.data.command, msg.uuid, msg.data.commandName);
                steps.setDisabled(true);
            }
            return command;
        }
        
        this.on(this.EVENTS.WS.MESSAGE, (event: MessageEvent) => {
            const msg = JSON.parse(event.data) as TSocketMessageData;

            if (isCommandMessage(msg)) {
                switch (msg.type) {
                    case 'COMMAND': {
                        break;
                    }
                    case 'COMMAND_LOG': {
                        const command = getCommand(msg);
                        if (command) {
                            command.addLog(msg.data.text);
                        }
                        break;
                    }
                }
                if (msg.status === 'DONE' && msg.error) {
                    const command = getCommand(msg);
                    if (command) {
                        command.addLog(msg.error);
                    }
                }
            }

            if (isTargetAliveMessage(msg)) {
                app.setTargetIsAlive(msg.status);
            }
        });

    }

    destroy() {
        this.socket.close();
    }

}

function getInstance(options?: SocketOpts): AppSocket {
    if (!instance) {
        instance = new AppSocket(options);
    }
    return instance;
};

function destroyInstance() {
    if (instance) {
        instance.destroy();
        instance = null;
    }
};

// TCommandMessageData | TTargetAliveMessageData | TServerInfoMessageData;
function isCommandMessage(msg): msg is TCommandMessageData {
    if (['COMMAND', 'COMMAND_LOG'].indexOf(msg.type) >= 0) {
        return true;
    }
    return false;
}

function isTargetAliveMessage(msg): msg is TTargetAliveMessageData {
    if (['TARGET_ALIVE'].indexOf(msg.type) >= 0) {
        return true;
    }
    return false;
}