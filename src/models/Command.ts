import {observable, computed, action, IObservableArray} from 'mobx';
import CommandsStore from '../stores/CommandsStore';
import Rest from '../service/Rest';
import { generateCid, generateUUID } from '../../utils/utils';

export interface ICommandOpts {
	cmd: string;
	commandName: string;
	uuid?: string;
}

export default class Command {
	
	static create = create;

	private _store: CommandsStore;
	private _rest: Rest;

	@observable
	private _logs = [];

	cid: string;
	commandName: string;
	uuid: string;
	cmd: string;

	constructor(store: CommandsStore, options: ICommandOpts) {
		this.cid = generateCid('command');
		this.uuid = options.uuid || generateUUID();
		this.cmd = options.cmd;
		this.commandName = options.commandName;
		this._rest = new Rest();
		this._store = store;
	}

	run() {
		return this._rest.post('/deploy', {
			body: {
				uuid: this.uuid,
				command: this.cmd,
				commandName: this.commandName
			}
		});
	}

	@action addLog(log: string) {
		this._logs.push(log);
	}

	@action clearLogs() {
		this._logs = [];
	}

	getLogs(): Array<string> {
		return this._logs
			.map(item => item.toString())
			.filter(text => text !== String.fromCharCode(10));
	}

	destroy() {
		this._store.removeCommand(this);
	}
}

function create(store, options: ICommandOpts) {
	return new Command(store, options);
}
