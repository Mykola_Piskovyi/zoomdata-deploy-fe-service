import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { AppBar, IconButton } from 'material-ui';
import { DeviceStorage } from 'material-ui/svg-icons';
import { MuiThemeProvider, getMuiTheme, colors } from 'material-ui/styles';

import './App.scss';

import CommandsStore from './stores/CommandsStore';
import StepsStore from './stores/StepsStore';
import AppStore from './stores/AppStore';
import RedeployView from './components/RedeployView/RedeployView';
import TargetDetails from './components/TargetDetails/TargetDetails';

interface IAppProps {
    commandsStore: CommandsStore;
    stepsStore: StepsStore;
    appStore: AppStore;
}

export class App extends React.Component<IAppProps, {}> {

    render() {

        const redeployProps = {
            commandsStore: this.props.commandsStore,
            stepsStore: this.props.stepsStore,
            appStore: this.props.appStore,
        };

        return (
            <MuiThemeProvider muiTheme={customizeTheme()}>
                <div className='Application'>
                    <AppBar title='ZD Deploy Service' iconElementLeft={<div/>}/>
                    <div className='app-main-container'>
                        <RedeployView {...redeployProps}/>
                    </div>
                </div>
            </MuiThemeProvider>
        );
    }

}

function customizeTheme() {
    return getMuiTheme({
        
    });
}