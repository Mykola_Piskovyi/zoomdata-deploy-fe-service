var path = require('path');
var fs = require('fs');
var shelljs = require('shelljs');
var webpack = require('webpack');
var nodeExternals = require('webpack-node-externals');
var Clean = require('clean-webpack-plugin');
var Copy = require('copy-webpack-plugin');
var UglifyJSPlugin = require('uglifyjs-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var TARGET_POINT = process.env.TARGET_POINT;
var NODE_MODE = process.env.NODE_ENV || 'development'; 
var IS_PROD = NODE_MODE === 'production';

var INPUT_CLIENT = path.resolve(__dirname, './src/index.ts');
var INPUT_SERVER = path.resolve(__dirname, './server/server.ts');
var OUT_PATH = 'dist';
var OUTPUT_CLIENT = 'app.bundle.js';
var OUTPUT_VENDOR_CLIENT = 'vendor.bundle.js';
var OUTPUT_SERVER = 'server.bundle.js';

var clientPolyfill = ['whatwg-fetch'];

var resolve = {
    extensions: ['.js', '.ts', '.tsx']
};

var plugins = [
    new webpack.DefinePlugin({
        'process.env': {'NODE_ENV': JSON.stringify(NODE_MODE)}
    })
];

console.log('process.env.NODE_ENV: ' + NODE_MODE);

// ----- CONFIGS

var clientConfig = {
    target: 'web',
    entry: {
        app: [...clientPolyfill, INPUT_CLIENT],
        vendor: ['lodash', 'react', 'react-dom', 'mobx', 'mobx-react', 'material-ui']
    },
    output: {
        path: path.resolve(OUT_PATH),
        filename: OUTPUT_CLIENT
    },
    devtool: IS_PROD ? 'nosources-source-map' : 'eval-source-map',
    module: {
        rules: [{
            test: /\.tsx?$/,
            exclude: /node_modules/,
            loader: 'ts-loader',
        }, {
            test: /\.scss$/,
            exclude: /node_modules/,
            loader: ExtractTextPlugin.extract({fallback: 'style-loader', use: 'css-loader!sass-loader'})
        }]
    },
    resolve: resolve,
    plugins: plugins
        .concat([
            new Clean([OUT_PATH]),
            new Copy([{ from: './index.html' }]),
            new webpack.optimize.CommonsChunkPlugin({name: 'vendor', filename: OUTPUT_VENDOR_CLIENT}),
            new ExtractTextPlugin({filename: 'main.css', allChunks: true})
        ])
};

var serverConfig = {
    target: 'node',
    entry: INPUT_SERVER,
    externals: [nodeExternals()],
    context: __dirname,
	node: {
		__filename: true,
		__dirname: true
	},
    output: {
        path: path.resolve(__dirname, OUT_PATH),
        filename: OUTPUT_SERVER,
        libraryTarget: 'commonjs'
    },
    devtool: 'eval-source-map',
    module: {
        rules: [{
            test: /\.ts$/,
            exclude: path.resolve(__dirname, 'node_modules'), // /node_modules/,
            loader: 'ts-loader'
        }]
    },
    resolve: resolve,
    plugins: plugins.concat([
        new webpack.BannerPlugin({
            banner: '#!/usr/bin/env node',
            raw: true
        }),
        function() {
            this.plugin('done', () => setChmodToServerFile());
        }
    ])
};

var exports =  [clientConfig, serverConfig];

if (TARGET_POINT === 'SERVER') {
    exports = [serverConfig];
}
if (TARGET_POINT === 'CLIENT') {
    exports = [clientConfig];
}

module.exports = exports;

function setChmodToServerFile(filePath) {
    const serverFile = path.resolve(__dirname, `${OUT_PATH}/${OUTPUT_SERVER}`);
    shelljs.chmod('+x', serverFile);
}
